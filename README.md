- Copy `cp.ps1` to a folder, for example: `C:\users\user\cp.ps1` - avoid spaces!

- Open FastStone Image Viewer

- Open Settings (F12) 

- Select Programs

- Click Add

- Select `C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe`

- Set Parameters as `C:\users\user\cp.ps1 (filename)`

- Set Execution Policy

```sh
Set-ExecutionPolicy -Scope CurrentUser AllSigned
```

- Sign script

https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_signing?view=powershell-7.4

_Import the certificate to Trusted Root Certification Authorities_

- View an Image

- Press `E` to launch script
