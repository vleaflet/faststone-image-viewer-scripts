param(
    [Parameter(Mandatory = $true)]
    [ValidateScript({ Test-Path -Path $_ -Type Leaf })]
    [string]$filePath
)

$targetFolder = "C:\temp"

if (-not (Test-Path -Path $targetFolder)) {
    Write-Host "Folder '$targetFolder' does not exist. Exiting script."
    Write-Host "Press any key to continue..."
    $null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    exit
}

$sourceFileName = [System.IO.Path]::GetFileName($filePath)
$sha256Hash = (Get-FileHash -Path $filePath -Algorithm SHA256).Hash
$extension = [System.IO.Path]::GetExtension($filePath)
$targetFileName = "${sha256Hash}${extension}"
$targetFilePath = Join-Path -Path $targetFolder -ChildPath $targetFileName

if (Test-Path -Path $targetFilePath -Type Leaf) {
    Write-Host "The target file '$targetFilePath' already exists. Exiting script."
    Write-Host "Press any key to continue..."
    $null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    exit
}

try {
    Copy-Item -Path $filePath -Destination $targetFilePath -ErrorAction Stop
    Write-Host "Copied '$sourceFileName' to '$targetFilePath'."
}
catch {
    Write-Host "An error occurred while copying the file."
    Write-Host "Press any key to continue..."
    $null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    exit
}
